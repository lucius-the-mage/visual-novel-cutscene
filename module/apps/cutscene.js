export default class Cutscene extends Application {
    static inst;

    constructor(options) {
        super(options);

        this.actors = [];
        this.margin = 10;
        this.hidden = true;

        game.socket.on("module.vn-cutscene", this._handleSocketMessage.bind(this));
    }

    async _handleSocketMessage(command) {
        switch (command.command) {
            case "show":
                this.show();
                break;
            case "hide":
                this.hide();
                break;
            case "margin-change":
                this.margin = command.data;
                break;
            case "remove-actor":
                this.removeActor(command.data);
                break;
            case "add-actor":
                this.addActor(command.data);
                break;
            case "render":
                this.render();
                break;
        }
    }

    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            dragDrop: [{ dropSelector: ".items" }],
            id: "vn-cutscene",
            template: "modules/vn-cutscene/templates/cutscene.html",
            popOut: false
        });
    }

    async _onDrop(event) {
        const data = TextEditor.getDragEventData(event);

        if (data.type !== "Actor") return;
        const a = await Actor.implementation.fromDropData(data);
        this.addActor(a);
    }

    removeActor(actor) {
        const removeIndex = this.actors.findIndex(a => a.side === actor.side && a._id === actor._id);
        if (removeIndex === -1) return;
        this.actors.splice(removeIndex, 1);
    }

    addActor(actor) {
        this.actors.push(actor);
    }

    hide() {
        this.hidden = true;
    }

    show() {
        this.hidden = false;
    }

    getData() {
        const data = super.getData();

        data.actorsLeft = this.actors.filter(a => a.side === "left").reverse();
        data.actorsRight = this.actors.filter(a => a.side === "right").reverse();
        data.margin = -this.margin;
        data.padding = this.margin / 2;
        data.hidden = this.hidden;

        return data;
    }
}

Hooks.on("ready", function () {
    Cutscene.inst = new Cutscene();
    Cutscene.inst.render(true);
});