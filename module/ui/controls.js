import CutsceneDialog from "../apps/cutscene-dialog.js";

Hooks.on("getSceneControlButtons", function (controls) {
    if (canvas == null) { return };

    const tokensIndex = controls.findIndex(c => c.name === "token");
    if (tokensIndex === -1) { return; }

    const tokenControl = controls[tokensIndex];
    tokenControl.tools.push({
        name: "show-controls",
        title: "CONTROLS.MeasureClear",
        visible: game.user.isGM,
        icon: "fas fa-tv",
        onClick: () => (new CutsceneDialog()).render(true),
        button: true
    });
});