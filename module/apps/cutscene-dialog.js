import Cutscene from "./cutscene.js";

export default class CotsceneDialog extends Application {
    constructor(options = {}) {
        super(options);
    }
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            dragDrop: [{ dropSelector: ".actors", dragSelector: ".actor" }],
            id: "vn-cutscene-dialog",
            template: "modules/vn-cutscene/templates/cutscene-dialog.html",
            height: 300,
            width: 460,
            resizable: true,
        });
    }

    get cutscene() {
        return Cutscene.inst;
    }

    activateListeners(html) {
        super.activateListeners(html);

        html.find(".cutscene-dialog-actor-active").change(this._onActorActive.bind(this));
        html.find(".delete-actor").click(this._onDeleteActor.bind(this));
        html.find(".margin").change(this._onMarginChange.bind(this));
        html.find(".hide-cutscene").click(this._onCutsceneHide.bind(this));
        html.find(".show-cutscene").click(this._onCutsceneShows.bind(this));
    }

    async _sendEvent(data) {
        game.socket.emit("module.vn-cutscene", data);
        await this.cutscene._handleSocketMessage(data);
    }

    _onCutsceneShows(event) {
        this._sendEvent({ command: "show" });
        this._sendEvent({ command: "render" });
    }

    _onCutsceneHide(event) {
        this._sendEvent({ command: "hide" });
        this._sendEvent({ command: "render" });
    }

    _onMarginChange(event) {
        const margin = parseInt(event.target.value);

        this._sendEvent({ command: "margin-change", data: margin });
        this._sendEvent({ command: "render" });
    };

    _onDeleteActor(event) {
        event.preventDefault();
        const actorBlock = event.currentTarget.closest(".actor");
        const actorId = actorBlock.dataset.actorId;
        const side = actorBlock.dataset.side;

        this._sendEvent({ command: "remove-actor", data: { _id: actorId, side: side } });
        this._sendEvent({ command: "render" });

        this.render();
    }

    _onActorActive(event) {
        const actorIndex = event.currentTarget.dataset.actorIndex;
        const side = event.currentTarget.closest(".actors").dataset.side;
        const checked = event.currentTarget.checked;

        const shiftPressed = game.keyboard.isModifierActive(KeyboardManager.MODIFIER_KEYS.SHIFT);

        if (shiftPressed) {
            this.cutscene.actors.forEach(a => {
                if (a.side !== side) return;
                a.active = false;
            });
        }
        this.cutscene.actors.filter(a => a.side === side)[actorIndex].active = checked || shiftPressed;

        this.cutscene.render(true);
        this.render(true);
    }

    async _onDragStart(event) {
        const dragData = {
            type: "Actor",
            id: event.currentTarget.dataset.actorId,
            _id: event.currentTarget.dataset.actorId,
            side: event.currentTarget.dataset.side,
        }
        event.dataTransfer.setData("text/plain", JSON.stringify(dragData));
    }

    async _onDrop(event) {
        const side = event.currentTarget.closest(".actors").dataset.side;
        const data = TextEditor.getDragEventData(event);

        if (data.type !== "Actor") return;
        this._sendEvent({ command: "remove-actor", data: data });

        const a = await Actor.implementation.fromDropData(data);
        const ad = a.toObject()
        ad.side = side;

        this._sendEvent({ command: "add-actor", data: ad });
        this._sendEvent({ command: "render" });

        this.render(true);
    }

    getData() {
        const data = super.getData();

        data.actorsLeft = this.cutscene?.actors.filter(a => a.side === "left") || [];
        data.actorsRight = this.cutscene?.actors.filter(a => a.side === "right") || [];
        data.margin = this.cutscene?.margin || 10;

        return data;
    }
}